<html>
    <head>
        <title>Form Sign Up</title>
        <h1>Buat Account Baru!</h1>
    </head>

    <body>
        <form action="/welcome" method="POST">
        {{ csrf_field() }}
        <div>
            <h3>Sign Up Form</h3>
            <label for="firstname">First Name:</label>
            <br><br>
            <input type="text" id="firstname" name='fname'>
            <br><br>
            <label for="lastname">Last Name:</label>
            <br><br>
            <input type="text" id="lastname" name="lname"> 
            <br><br>
        </div> 
        
        <div>
            <label>Gender</label>
            <br>
            <input type="radio" name="gender" value="0">Male <br>
            <input type="radio" name="gender" value="1">Female <br>
            <input type="radio" name="gender" value="3">Others
            <br><br>
        </div>
        
        <div>
            <label>Nationality:</label>
            <br><br>
            <Select>
                <option value="ind">Indonesian</option>
                <option value="sing">Singaporean</option>
                <option value="mal">Malaysian</option>
                <option value="aus">Australian</option>
            </Select>
            <br><br>
        </div>
        
        <div>
            <label>Languange Spoken:</label>
            <br><br>
            <input type="checkbox" value="bahasa">Bahasa Indonesia <br>
            <input type="checkbox" value="English">English <br>
            <input type="checkbox" value="Other">Other 
            <br><br>
        </div>

        <div>
            <label id="bio">Bio:<label>
            <br><br>
            <textarea name="Bio" id="bio" cols="30" rows="10"></textarea>
            <br>
        </div>

        <div>
            <input type="submit" value="Sign Up">
        </div>
    </form>
    </body>
</html>